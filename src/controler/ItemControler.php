<?php


namespace mywishlist\controler;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\vue\VueItem;
use mywishlist\vue\VueListe;


/**
 * Correspondant au controler pour les items (ajout / affichage / creation ect..)
 */
class ItemControler
{
    private $token;
    public function reservItem($rq, $rs, $args)
    {
        //selection de l item a reserver
        $id = $args['id'];
        $item = Item::where('id', '=', $id)->first();


        $vue = new \mywishlist\vue\VueParticipant($item,$rq->getUri()->getBasePath());
        //tester la possibilité de reserver l'item
        if ($item->reservation == 0) {
            //afficher l'état de réservation de l'item
            $rs->getBody()->write($vue->render(4));
            //mise de l item en question sous reservation
            $item->reservation = '1';
            $item->save();
        } else {
            //afficher un message d'erreur pour prevenir une reservation déja existante
            $rs->getBody()->write($vue->render(5));
        }


        return $rs;
    }
    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de creer un item
     */
    function creerItem($rq, $rs,$args)
    {
        $item = new Item();
        $token = $args['token'];
        $vue = new VueItem(null,$rq->getUri()->getBasePath(),null,$token);
        if (!isset($_POST['nom']))
            $vue->error("veuillez entrer un nom");
        if (!isset($_POST['descr']))
            $vue->error("veuillez entrer une description");
        if (!isset($_POST['tarif']))
            $vue->error("Le tarif est manquant");
        $nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $descr = filter_var($_POST['descr'], FILTER_SANITIZE_STRING);
        $tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $liste = Liste::where('token', '=', $token)->first();
        $item->liste_id =$liste->no;
        $item->nom = $nom;
        $item->descr = $descr;
        $item->tarif = $tarif;
        try {
            $item->save();
            return $rs->withRedirect($rq->getUri()->getBasePath() . "/affichage/liste/$liste->partage/$liste->token",301);
        } catch (QueryException $e) {
            return $vue->error('Problème durant la creation de la liste');
        }
    }
    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet d'afficher le formulaire
     */
    public function getFormItem($rq, $rs, $args)
    {
        $token = $args['token'];
        $vue = new VueItem(null,$rq->getUri()->getBasePath(),null, $token);
        $liste = Liste::where('token', '=', $token)->first();
        if (!isset($liste)){
            $rs= $vue->error('liste introuvable');
        }
        else{
            $rs->getBody()->write($vue->render(1));
        }
        return $rs;
    }

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de récupérer un item selon son id
     */
    public function getItem($rq, $rs, $args)
    {
        $id = $args['id'];

        //Verification du paramètre
        $options = array(
            'options' => array(
                'default' => 1, // valeur à retourner si le filtre échoue
                // autres options ici...
                'min_range' => 1
            ),
            'flags' => FILTER_FLAG_ALLOW_OCTAL,
        );
        $id = filter_var($id, FILTER_VALIDATE_INT, $options);

        //Affichage des items $q2 = Club::where( 'id', '=', 12 ) ;
        $items = Item::where('id', '=', (int)$id)->first();
        $vue = new VueItem(null,$rq->getUri()->getBasePath(),$items,null);
        if (!isset($items)){
            $rs= $vue->error('item introuvable');
        }
        else{
            $rs->getBody()->write($vue->render(2));
        }
        return $rs;
    }

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet d'ajouter l'url à un item
     */
    public function addImg($rq, $rs, $args)
    {
        $id = $args['id'];
        $img= $args['img'];
        $item = Item::where('id', '=', $id)->first();
        $vue = new \mywishlist\vue\VueParticipant($item,$rq->getUri()->getBasePath());
        $item->img = $img;
        //affiche l'url de l'image de l'item
        $rs->getBody()->write($vue->render(6));
        $item->save();
        return $rs;
    }

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de supprimer un item
     */
    public function delItem($rq,$rs,$args)
    {
        $id = $args['id'];
        Item::destroy($id);
        $items = Item::all();
        $vue = new \mywishlist\vue\VueParticipant($items,$rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(2));
        return $rs;
    }
    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de supprimer une image
     */
    public function delImg($rq,$rs,$args){
        $id = $args['id'];
        $item = Item::where('id', '=', $id)->first();
        $vue = new \mywishlist\vue\VueParticipant($item,$rq->getUri()->getBasePath());
        $item->url = null;
        $item->img = null;
        //affiche l'url de l'image de l'item
        $rs->getBody()->write($vue->render(6));
        $item->save();
        return $rs;
    }
}