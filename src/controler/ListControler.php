<?php

namespace mywishlist\controler;

use Illuminate\Database\QueryException;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\vue\VueListe;

/**
 * Correspondant au controler pour les listes (ajout / affichage / creation ect..)
 */
class ListControler
{

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     *  Permet de récupérer et d'afficher les listes de souhaits
     */
    public function getListes( $rq, $rs, $args ) {
        //Récupération des listes dans la BDD
        $listeS = Liste::get();
        //Création de la vue associé
        $vue = new \mywishlist\vue\VueParticipant($listeS,$rq->getUri()->getBasePath());
        //Utilisation de la vue pour l'affichage des listes de souhait
        $rs->getBody()->write($vue->render(1));
        return $rs ;
    }
    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     *  Permet de récupérer les informations des listes
     */
    public function getInfoListe($rq , $rs, $args){
        //Liste dans la bb
        $token = $args['token'];
        $liste = Liste::where('token', '=', $token)->first();
        $vue = new \mywishlist\vue\VueListe(null,$rq->getUri()->getBasePath(),$liste);
        $rs->getBody()->write($vue->render(3));
        return $rs;
    }

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de récupérer et d'afficher les items d'une liste
     */
    public function getListeItems($rq, $rs , $args){
        $numPartage = $args['partage'];
        $listeTab = Liste::where('partage', '=', $numPartage)->first();
        $listeno = $listeTab->no;
        if($listeno > 0){
            $items = Item::where('liste_id', '=', (int)$listeno)->get();
            $vue = new \mywishlist\vue\VueParticipant($items,$rq->getUri()->getBasePath());
            $rs->getBody()->write($vue->render(2));
        }else{
            $vue = new \mywishlist\vue\VueParticipant(null,$rq->getUri()->getBasePath());
            $rs->getBody()->write($vue->error("La liste n'existe pas"));
        }
        return $rs;
    }

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de supprimer un item d'une liste
     */
    public function supprItemFromListe($rq,$rs, $args)
    {
        $liste = $args['token'];
        $id = $args['id'];


        Item::where('id', $id)->update(['liste_id' => 0]);
        $vue = new \mywishlist\vue\VueParticipant($liste, $rq->getUri()->getBasePath());
        $rs->getBody()->write($vue->render(7));
        return $rs;
    }

    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     * Permet de récupérer et d'afficher les items d'une liste en étant le créateur
     */
    public function getListeItemsCreateur($rq, $rs , $args){
        $numPartage = $args['partage'];
        $liste = Liste::where('partage', '=', $numPartage)->first();
        $listeid = $liste->no;
        if($args['token'] == $liste->token){
            $items = Item::where('liste_id', '=', (int)$listeid)->get();
            $vue = new \mywishlist\vue\VueListe($items,$rq->getUri()->getBasePath(), $liste);
            $rs->getBody()->write($vue->render(2));
        }else {
            $rs = $this->getListeItems($rq, $rs , $args);
        }
        return $rs;
    }

    public function getFormListe($rq, $rs, $args){
        $vue = new VueListe(null,$rq->getUri()->getBasePath(),null);
        $rs->getBody()->write($vue->render(1));
        return $rs;
    }
    /**
     * @param $rq
     * @param $rs
     * @param $args
     * @return mixed
     *  Permet de creer une liste
     */
    public function createListe($rq, $rs, $args){
        $vue = new VueListe(null,$rq->getUri()->getBasePath(),null);

        //Gestion de la date d'expiration
        $expiration = date('Y-m-d', strtotime($_POST['dateE']));
        $time = (time() - strtotime('d-m-Y', $expiration));
        if($expiration == null ||  $time < 0){
            $vue->error("Problème avec la date entrée");
            $rs->getBody()->write($vue->render(1));
        }else{
            $liste = new Liste();
            $liste->user_id = null;
            $liste->titre = filter_var($_POST['titre'],FILTER_SANITIZE_STRING);
            $liste->description = filter_var($_POST['desc'],FILTER_SANITIZE_STRING);
            $liste->expiration = $expiration;
            $liste->partage = crypt( $_POST['titre']. $_POST['desc'], 'partageSalt');
            $liste->token = crypt( $_POST['titre']. $_POST['desc'], 'tokenSalt');
            try {
                if($liste->save()){
                    return $rs->withRedirect($rq->getUri()->getBasePath() . "/affichage/liste/info/$liste->token",301);
                }else{
                    $vue->error('Impossible de creer la liste');
                    $rs->getBody()->write($vue->render(1));
                }
            }catch (QueryException $e){
                $vue->error('Problème durant la creation de la liste');
                $rs->getBody()->write($vue->render(1));
            }
        }
        return $rs;
    }
}