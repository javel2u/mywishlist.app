<?php

namespace mywishlist\vue;
/**
 * Class VueListe
 * @package mywishlist\vue
 */
class VueItem extends VueParticipant
{

    private $item;
    private $token;

    /**
     * VueParticipant constructor.
     *  reçoit en paramètre un tableau d'objets (items, listes) à afficher. Lorsqu'il faut
     * afficher 1 seul objet, ce tableau contient 1 seule entrée.
     * @param $t
     */
    public function __construct($t, $b, $i, $tok)
    {
        parent::__construct($t, $b);
        $this->item = $i;
        $this->token = $tok;
    }

    /**
     * @param $selec int Selection de la vue
     * @return string html a afficher
     */
    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 1 :
            {
                $content .= $this->rendreFormCreationItem();
                break;
            }
            case 2:
            {
                $content .= $this->afficherItem();
                break;
            }
            case 3:
            {
                $content .= $this->afficherInfoItem();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    public function rendreFormCreationItem()
    {
        return <<<HTML
$this->error
<form action="$this->basePath/ajouter/item/$this->token" method="post">
    <div>
        <label for="nom">Nom</label>
        <input type="nom" id="nom" name="nom" required>
    </div>
    <label for="descr">Description</label>
        <input type="descr" id="descr" name="descr"required>
    <div>
        <label for="prix">Prix</label>
        <input type="number" step="0.01" id="tarif" name="tarif"required>
    </div>
    <input type='submit' value='Ajouter Item'>
</form>
HTML;
    }

    public function afficherInfoItem()
    {
        $res = '<strong>Vous venez de creer un Item :</strong><br>';
        $res .= "Nom de l'item : " . $this->item->nom . "<br>";
        $res .= "Description : " . $this->item->descr . "<br>";
        $res .= "Tarif : " . $this->item->tarif . "<br>";
        return $res;
    }

    public function afficherItem()
    {
        $res = '<strong>Ceci est votre Item: </strong><br>';
        $res .= "Item n°" . $this->item->id . "<br>";
        $res .= "Nom de l'item : " . $this->item->nom . "<br>";
        $res .= "Description : " . $this->item->descr . "<br>";
        $res .= "Tarif : " . $this->item->tarif . "<br>";
        return $res;
    }
    public function error($msg)
    {
        $res= $this->entete();
        $res .= 'Error : ' . " $msg";
        $res .=$this->bas();
        return $res;
    }
}