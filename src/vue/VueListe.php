<?php

namespace mywishlist\vue;
/**
 * Class VueListe
 * @package mywishlist\vue
 */
class VueListe extends VueParticipant
{

    private $liste;

    /**
     * VueParticipant constructor.
     *  reçoit en paramètre un tableau d'objets (items, listes) à afficher. Lorsqu'il faut
     * afficher 1 seul objet, ce tableau contient 1 seule entrée.
     * @param $t
     */
    public function __construct($t,$b,$l)
    {
        parent::__construct($t,$b);
        $this->liste = $l;
    }

    /**
     * @param $selec int Selection de la vue
     * @return string html a afficher
     */
    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 1 :
            {
                $content .= $this->rendreFormCreation();
                break;
            }
            case 2:
            {
                $content .= $this->afficherListe();
                break;
            }
            case 3:
            {
                $content .= $this->afficherInfoListe();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    public function rendreFormCreation()
    {
        return <<<HTML
$this->error
<form action="$this->basePath/creation/liste" method="post">
    <div>
        <label for="titre">Titre</label>
        <input type="titre" id="titre" name="titre" required>
    </div>
    <div>
        <label for="desc">Description</label>
        <input type="desc" id="desc" name="desc">
    </div>
    <div>
        <label for="dateE">Date d'expiration :</label>
        <input id='dateE' name='dateE' type='date' value='' required>
    </div>
    <input type='submit' value='Créer liste'>
</form>
HTML;
    }

    public function afficherInfoListe(){
        $res = '<strong>Vous venez de creer une liste :</strong><br>';
        $res .= "Titre de la liste : " . $this->liste->titre . "<br>";
        $res .= "Description : " . $this->liste->description . "<br>";
        $res .= "Date d'expiration : " . $this->liste->expiration . "<br>";
        $par = $this->liste->partage;
        $route = "$this->basePath/affichage/liste/$par";
        $token = $this->liste->token;
        $res .= "Lien de partage : <a href='$route'>$route</a><br>";
        $res .= "Lien de modification : <a href='$route/$token'>$route/$token</a>";
        return $res;
    }

    public function afficherListe(){
        $token = $this->liste->token;
        $res =  '<button onclick="location.href=\'' . $this->basePath . "/ajouter/item/$token'\"" . 'type="button" class="btn btn-danger">Ajouter item</button>';
        $res .= '<strong>Ceci est votre liste: </strong><br>';
        $res .= "Items de la liste n°" . $this->liste->no . "<br>";
        foreach ($this->tab as $item) {
            $res .= $item->id . ' : ' . $item->nom . ' : ' . $item->descr . '<br>';
        }
        $res .= '<br>';
        $par = $this->liste->partage;
        $route = "$this->basePath/affichage/liste/$par";
        $res .= "Lien de partage : <a href='$route'>$route</a>";
        return $res;
    }
}