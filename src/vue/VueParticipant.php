<?php
namespace mywishlist\vue;
use mywishlist\models\Liste;

/**
 * Class VueParticipant
 * @package mywishlist\vue
 */
class VueParticipant
{
    /**
     * @var array
     * tableau d'objets (items, listes)
     */
    protected $tab;
    /**
     * @var string
     * base path de la requete
     */
    protected $basePath;
    protected $error;

    /**
     * Constante correspondant a la selection de la vue
     * Affichage de la liste des listes de souhaits (1) , l'affichage d’une liste de souhaits et
     * ses items (2), l'affichage d'un item (3)
     */
    //private const LIST_VIEW = 1;
    //private const LIST_ITEM_VIEW = 2;
    //private const ITEM_VIEW = 3;


    /**
     * VueParticipant constructor.
     *  reçoit en paramètre un tableau d'objets (items, listes) à afficher. Lorsqu'il faut
     * afficher 1 seul objet, ce tableau contient 1 seule entrée.
     * @param $t
     */
    public function __construct($t,$b)
    {
        $this->tab = $t;
        $this->basePath = $b;
        $this->error = '';
    }

    /**
     * Une méthode pour l'affichage général, recevant en paramètre un sélecteur de mode d’affichage qui
     * permet de choisir l'affichage de la liste des listes de souhaits (1) , l'affichage d’une liste de souhaits et
     * ses items (2), l'affichage d'un item (3), la reservation d'in item (4), un problème de reservation(5). Cette méthode est publique, et produit et affiche une page html complete
     * @param $selec int Selection de la vue
     * @return string html a afficher
     */
    public function render($selec)
    {
        $content = $this->entete();
        switch ($selec) {
            case 1 :
            {
                $content .= $this->htmlListSouhait();
                break;
            }
            case 2 :
            {
                $content .= $this->htmlListSouhaitItem();
                break;
            }
            case 3 :
            {
                $content .= $this->htmlListSouhaitItemCrea();
                break;
            }
            case 4 :
            {
                $content.=$this->htmlReservItem();
                break;
            }
            case 5 :
            {
                $content.=$this->htmlReservItemProb();
                break;
            }
            case 6 :
            {
                $content.=$this->htmlAjoutImage();
                break;
            }
            case 7 :
            {
                $content.=$this->htmlItemSuppr();
                break;
            }
        }
        $content .= $this->bas();
        return $content;
    }

    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage d'une liste de souhait
     */
    public function htmlListSouhait(){
        $res = '<button onclick="location.href=\''. $this->basePath . '/creation/liste\'" type="button" class="btn btn-success">Créer une liste</button><br>';
        foreach ($this->tab as $sou) {
            $res .= $sou->no . ' : ' . $sou->titre . ' ; ' . $sou->description . '<br>';
        }
        return $res;
    }

    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage des items d'une liste
     */
    public function htmlListSouhaitItem(){
        $res = '';
        $res .= "Items de la liste n°" . $this->tab[0]->liste_id . "<br>";
        foreach ($this->tab as $item) {
            if($item->reservation == 0) {
                $res .= '<div class="card"><div class="card-body">' . $item->id . ' : ' . $item->nom . ' : ' . $item->descr . '         ' .
                    '<button onclick="location.href=\''. $this->basePath . '/reservation/item/' . $item->id . '\'" type="button" class="btn btn-danger">Réserver</button>' . '</div></div>' . '<br>';
            }else{
                $res .= '<div class="card"><div class="card-body">' . $item->id . ' : ' . $item->nom . ' : ' . $item->descr . '</div></div>' . '<br>';
            }
        }
        return $res;
    }

    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage des items d'une liste par un créateur
     */
    public function htmlListSouhaitItemCrea(){
        $res = '<strong>Ceci est votre liste: </strong><br>';
        $res .= "Items de la liste n°" . $this->tab[0]->liste_id . "<br>";
        foreach ($this->tab as $item) {
            $res .= '<div class="card"><div class="card-body">' . $item->id . ' : ' . $item->nom . ' : ' . $item->descr . '</div></div>' . '<br>';
        }
        return $res;
    }

    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage d'un item
     */
    public function htmlItem(){
        return $this->tab->id . ' : ' . $this->tab->liste_id . ' : ' . $this->tab->nom . ' : ' . $this->tab->descr . '<br>';
    }
    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage de l'état de réservation de l'item
     */
    public function htmlReservItem(){
        return $this->tab->id.' Item maintenant réservé'.'<br>';
    }
    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage d'un message prevenant d'une reservation déja existante
     */
    public function htmlReservItemProb(){

        return $this->tab->id.' déja réservé'.'<br>';
    }

    /**
     * @return string Affichage (contenu html)
     * Permet l'affichage de l'url d'un item
     */
    public function htmlAjoutImage(){
        return $this->tab->id . ':' . $this->tab->nom . '  url: ' . $this->tab->url . '<br>';
    }


    /**
     * @return string Affichage (contenu html=
     * Permet l'affichage de la suppression d'un item
     */
    public function htmlItemSuppr(){
        return 'Item supprimé de la liste <br>';
    }

    public function entete(){
        return <<<HTML
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Accueil MyWishList</title>
        <link rel="stylesheet" href="$this->basePath/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link href="$this->basePath/css/index.css" rel="stylesheet">
    </head>
    <body>


        <nav class="navbar navbar-expand-lg navbar-light bg-light navigateur">
            <a class="navbar-brand" href="$this->basePath/index.html">
                <img src="$this->basePath/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                MyWishList
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse p-2" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="$this->basePath/index.html">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="$this->basePath/affichage/souhaits">Listes</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Compte
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Se connecter</a>
                            <a class="dropdown-item disabled" href="#">Mes Listes</a>
                            <a class="dropdown-item disabled" href="#">Réservation</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item disabled" href="#">Se déconnecter</a>

                            <!-- commande a utiliser pour avoir acceder aux rubriques disabled quand on est connecté

                                $('.disabled').addClass('active').removeClass('disabled').html('On');

                            -->

                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Saisir" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
                </form>
            </div>
        </nav>

        <div class="container-fluid contenu">
HTML;
    }

    public function bas(){
        return <<<HTML
        <br><br><br><br><br><br>
        </div>

    </body>

    <footer class="page-footer font-small pt-3 ensemble_footer">
        <div class="container-fluid text-md-left bg-light haut_footer">
            <div class="row">
                <div class="col-md-6 mt-md-0 mt-3 noms">
                    <h6 class="text-uppercase">Participants du projet :</h6>
                    BARDET Brian, BEER Alexis, GROSJEAN Hervé, JAVEL Gaetan et SAKER Lucas.
                </div>
            </div>
        </div>
        <div class="footer-copyright text-center py-2 bas_footer">
            Projet 2019-2020 : MyWhishList
        </div>
    </footer>
</html>

HTML;
    }

    /**
     * @param $msg
     * @return string
     * Retourne une erreur
     */
    public function error($msg){
        return $this->entete() . $msg . $this->bas();
    }
}