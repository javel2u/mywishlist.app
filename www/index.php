<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Autoloader
 */
require_once '../src/vendor/autoload.php';

/**
 * Use
 */
use mywishlist\models\Liste;
use mywishlist\models\Item;

/**
 * Création connection avec la DB
 */
new mywishlist\db\Connection();


$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

/**
 * L'affichage d'une page d'accueil
 */
$app->get('/',
    function (Request $req, Response $resp, $args) {
        //Affichage de la page d'accueil
        $resp->getBody()->write(file_get_contents('index.html'));
        return $resp;
    }
)->setName('route_index');


/**
 * Liste
 */
$app->get('/creation/liste',
    function (Request $req, Response $resp, $args) {
        //Affichage de la liste
        $lc = new \mywishlist\controler\ListControler();
        return $lc->getFormListe($req, $resp, $args);
    }
)->setName('route_creation_liste');

$app->post('/creation/liste',
    function (Request $req, Response $resp, $args) {
        //Creation de la liste
        $lc = new \mywishlist\controler\ListControler();
        return $lc->createListe($req, $resp, $args);
    }
)->setName('route_creation_liste_post');

$app->get('/affichage/souhaits',
    function (Request $req, Response $resp, $args) {
        //Affichage des listes de souhaits
        $lc = new \mywishlist\controler\ListControler();
        return $lc->getListes($req, $resp, $args);
    }
)->setName('route_souhaits');

$app->get('/affichage/liste/{partage}',
    function (Request $req, Response $resp, $args) {
        //l'affichage de la liste des items d'une liste de souhaits avec lien de partage
        $lc = new \mywishlist\controler\ListControler();
        return $lc->getListeItems($req, $resp, $args);
    }
)->setName('route_items_liste_partage');

$app->get('/affichage/liste/info/{token}',
    function (Request $req, Response $resp, $args) {
        //Affichage des info de la liste
        $lc = new \mywishlist\controler\ListControler();
        return $lc->getInfoListe($req, $resp, $args);
    }
)->setName('route_items_liste_partage');

$app->get('/affichage/liste/{partage}/{token}',
    function (Request $req, Response $resp, $args) {
        //l'affichage de la liste des items d'une liste de souhaits avec lien de partage et le token (createur)
        $lc = new \mywishlist\controler\ListControler();
        return $lc->getListeItemsCreateur($req, $resp, $args);
    }
)->setName('route_items_liste_createur');

/**
 * Item
 */
$app->get('/affichage/item/{id}',
    function (Request $req, Response $resp, $args) {
        //l'affichage d'un item désignée par son ID
        $ic = new \mywishlist\controler\ItemControler();
        return $ic->getItem($req, $resp, $args);
    }
)->setName('route_item_id');

$app->get('/reservation/item/{id}',
    function (Request $req, Response $resp, $args) {
        //Reservation d un item
        $lc = new \mywishlist\controler\ItemControler();
        return $lc->reservItem($req, $resp, $args);
    }
)->setName('route_reserv_item');

$app->get('/ajouter/item/{token}',
    function (Request $req, Response $resp, $args) {
        $lc = new \mywishlist\controler\ItemControler();
        return $lc->getFormItem($req, $resp, $args);
    }
)->setName('route_itemAdd_get');
$app->post('/ajouter/item/{token}',
    function (Request $req, Response $resp, $args) {
        $lc = new \mywishlist\controler\ItemControler();
        return $lc->creerItem($req, $resp, $args);
    }
)->setName('route_itemAdd_post');


$app->get('/modifier/item/{id}/{img}',
    function (Request $req, Response $resp, $args) {

        //Affichage de la liste des souhaits
        $lc = new \mywishlist\controler\ItemControler();
        return $lc->addImg($req, $resp, $args);
    }
)->setName('route_addurl');

$app->get('/modifier/suppr/{token}/{id}',
    function (Request $req, Response $resp, $args) {

        //Affichage de la liste des souhaits
        $lc = new \mywishlist\controler\ListControler();
        return $lc->supprItemFromListe($req,$resp,$args);
    }
)->setName('route_suppr_item');

$app->get('/modifier/supprimg/{id}',
    function (Request $req, Response $resp, $args) {

        //Affichage de la liste des souhaits
        $lc = new \mywishlist\controler\ItemControler();
        return $lc->delImg($req, $resp, $args);
    }
)->setName('route_rmurl');

$app->run();